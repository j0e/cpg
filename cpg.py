#!/usr/bin/env python

#Import the library
from midiutil.MidiFile import MIDIFile
import sys, random, getopt
from analyser import *

class Generator:
	def __init__(self, chordtypes, ruletable, basenote, l, h, verbose):
		#self.currentbasenote=random.randint(l,h)
		self.basenote = basenote;
		self.currentchordtype=random.randint(1, len(chordtypes)-1)
		self.time = 0
		self.mf=MIDIFile(1)
		self.mf.addTempo(0,0,128)
		self.chordtypes=chordtypes
		self.ruletable=ruletable
		self.verbose = verbose
	def genchord(self):
		#global time, currentbasenote, currentchordtype
		chord=self.chordtypes[self.currentchordtype]
		self.currentbasenote = self.basenote+chord[0]
		#print chord
		if self.verbose:
			print "Generating chord type", self.currentchordtype, "with basenote", self.currentbasenote
		self.mf.addNote(0,0,self.currentbasenote,self.time,2,100)
		for i in range(1, len(chord)):
			self.mf.addNote(0,0,self.currentbasenote+chord[i],self.time,2,100)
		rule=self.ruletable[self.currentchordtype]
		nextchord=rule[random.randint(0, len(rule)-1)]
		if self.verbose:
			print nextchord
		self.currentbasenote+=nextchord[0]
		self.currentchordtype=nextchord[1]
		self.time+=2

def isnote(inputnote, testnote):
	return True if inputnote%12==testnote else False

def printusage():
	print "usage: cpg.py [-b <numberofbars>] [-i <inputfoldername>] [-o <outputfile>] [-v]"

def printhelp():
	printusage()
	print "longer help message goes here"

def main(argv):
	verbose = False
	outputfilename = "output.mid"
	inputfoldername = "midifiles"
	length = 8
	# first parse all command line arguments
	try:
		options, extraargs = getopt.getopt(argv, "hvo:i:b:")
	except getopt.GetoptError as e:
		print "cpg command line option error:", e.msg
		printusage()
		print "try 'cpg -h' for more information"
		sys.exit(2)
	for opt, arg in options:
		if opt == "-h":
			printhelp()
			sys.exit(2)
		elif opt == "-v":
			verbose = True
		elif opt == "-o":
			outputfilename = arg
		elif opt == "-i":
			inputfoldername = arg
		elif opt == "-b":
			length = int(arg)
	random.seed() # seed the random number generator
	a = Analyser(inputfoldername, verbose)
	a.analyse()
	g = Generator(a.chordtypes, a.ruletable, random.randint(a.l,a.h), a.l, a.h, verbose)
	for i in range(0,length):
		g.genchord()
	binfile = open(outputfilename, 'wb')
	g.mf.writeFile(binfile)
	binfile.close()

if __name__ == "__main__": 
    main(sys.argv[1:]) 

