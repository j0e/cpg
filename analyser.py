import testrules, os
from midiutil import midiparse
from operator import itemgetter

def removeduplicates(seq): # from http://stackoverflow.com/questions/480214/how-do-you-remove-duplicates-from-a-list-in-python-whilst-preserving-order
    seen = set()
    seen_add = seen.add
    return [ x for x in seq if not (x in seen or seen_add(x))]

class Analyser:
	def __init__(self, inputfoldername, verbose):
		self.chordtypes = []
		self.ruletable = []
		self.l=127
		self.h=0
		self.verbose = verbose
		self.inputfoldername = inputfoldername
		self.minnotes = 4
	def analysefile(self, filename):
		mf = midiparse.MidiFile.get_from_file(self.inputfoldername + "/" + filename)
		currenttime = mf.tracks[0].events[0].time
		filechords = []
		basenotes = [] # for construction of the ruletable
		intervals = [] # a temporary list
		# print mf.tracks[1].events
		for event in mf.tracks[0].events:
			if event.type == "NOTE_ON":
				if event.time != currenttime and len(intervals)>0:
					# change set of midi notes to set of intervals from basenote
					basenote = min(intervals)
					basenotes.append(basenote)
					chord = [interval - basenote for interval in intervals if interval!=basenote]
					chord.sort()
					chord.insert(0, basenote - basenotes[0])
					filechords.append(tuple(chord))
					currenttime = event.time
					intervals = []
				intervals.append(event.pitch) 
		# add the last chord here
		basenote = min(intervals)
		basenotes.append(basenote)
		chord = [interval - basenote for interval in intervals if interval!=basenote]
		chord.sort()
		chord.insert(0, basenote - basenotes[0])
		filechords.append(tuple(chord))
		del chord
		self.chordtypes.extend(filechords)
		if self.verbose:
			print "before:",self.chordtypes;
		##self.chordtypes=list(set([tuple(chord) for chord in self.chordtypes])) # remove duplicates from the list by converting to a set and back
		#for chord in self.chordtypes:
		#	if len(chord) < self.minnotes:
		#		self.chordtypes.remove(chord)
		#		filechords.remove(chord) # hack
		self.chordtypes=removeduplicates(self.chordtypes)
		if basenotes[0]<self.l:
			self.l=basenotes[0]
		if basenotes[0]>self.h:
			self.h=basenotes[0]
		if self.verbose:
			print "after:",self.chordtypes
			print "Generating rule table."
			print "filechords for this file =", filechords
		# now make the rule table
		numchords = len(filechords) # cached to speed up below since it's called in a loop
		for i in range(numchords):
			chord = filechords[i]
			thischordindex = self.chordtypes.index(tuple(chord)) # will certainly be in there because it was added above and converting to a set and back ensures no duplicates
			if self.verbose:
				print "Chord index", thischordindex
			basenotedifference=basenotes[(i+1)%numchords]-basenotes[i]
			for rule in self.ruletable:
				if rule[0]==thischordindex: # if it's the entry that starts with this chord
					if self.verbose:
						print "this one already exists", rule
					rule.append([basenotedifference, self.chordtypes.index(tuple(filechords[(i+1)%numchords]))])
					break
			else: # didn't find a matching rule, make one
				if self.verbose:
					print "creating a new rule for this one with number", thischordindex
				self.ruletable.append([thischordindex, [basenotedifference, self.chordtypes.index(tuple(filechords[(i+1)%numchords]))]])
		if self.verbose:
			print "Rule table after this file:"
			for i in range(len(self.ruletable)):
				print self.ruletable[i]
	def analyse(self):
		for filename in os.listdir(self.inputfoldername):
			if filename[-4:]==".mid":
				print "Analysing", filename 	 
				self.analysefile(filename)
		if self.verbose:
			print "Chord types:", self.chordtypes
		#print "Rule table:", self.ruletable
		self.ruletable = sorted(self.ruletable, key=itemgetter(0))
		for i in range(len(self.ruletable)):
			self.ruletable[i].pop(0)
			if self.verbose:
				print i, self.ruletable[i]
		#self.chordtypes = testrules.chordtypes
		#self.ruletable = testrules.ruletable
