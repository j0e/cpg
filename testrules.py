import json
#enums
def enum(**enums):
    return type('Enum', (), enums)

# Note names. Had to use S instead of # because it's the python comment character
N = enum(C=0, CS=1, Db=1, D=2, DS=3, Eb=3, E=4, F=5, FS=6, Gb=6, G=7, GS=8, Ab=8, A=9, AS=10, Bb=10, B=11, OCT=12)

# chord type constants
CT = enum(
	NONE=0,
	MAJOR=1, MINOR=2, MAJ79th=3, MIN79th=4,
	HENDRIX=5, SUS_11=6, SUS_11_5=7, MAJ6th=8,
	MIN79th_2=9, SUS_11_5_2=10, SUS_11_6_2=11, MAJ79th_2=12,
	SUS_11_6=13
)

# all chord intervals written in relation to C

chordtypes=[
	[12],
	
	[12, 12+N.E, 12+N.G],
	[12, 12+N.Eb, 12+N.G],
	[N.B, 12+N.D, 12+N.E, 12+N.G],
	[N.Bb, 12+N.D, 12+N.Eb, 12+N.G],
	
	[12, 12+N.E, 12+N.Bb, 24+N.Eb],
	[12, 12+N.F, 12+N.Bb, 24+N.D],
	[12, 12+N.F, 12+N.G, 12+N.Bb, 24+N.D],
	[N.G, 12+N.E, 12+N.G, 12+N.A, 24],
	
	[N.G, 12+N.D, 12+N.Eb, 12+N.G, 12+N.Bb],
	[N.G, N.Bb, 12+N.D, 12+N.F],
	[N.A, N.Bb, 12+N.D, 12+N.F],
	[N.G, 12+N.D, 12+N.E, 12+N.G, 12+N.B],
	
	[N.G, N.Bb, 12+N.D, 12+N.F, 12+N.A],
]
'''
ruletable = []

with open("rules.json", "r") as f:
	ruletable[:] = json.loads(f.read())

'''
ruletable=[
	[[5, CT.NONE], [-5, CT.NONE]],
	
	[[-5, CT.MAJOR], [-3, CT.MINOR], [4, CT.MINOR], [-7, CT.MAJOR], [7, CT.MAJOR], [5, CT.MAJOR]],
	[[-5, CT.MINOR], [-4, CT.MAJOR], [3, CT.MAJOR], [-7, CT.MINOR], [7, CT.MINOR], [5, CT.MINOR]],
	[[-4, CT.SUS_11_5], [-1, CT.HENDRIX]],
	[[-2, CT.MIN79th_2]], # [6, CT.MAJ6th], 
	
	[[5, CT.MIN79th]],
	[[-2, CT.SUS_11_5]],
	[[7, CT.MAJ79th_2], [5, CT.MAJ79th_2], [3, CT.MAJ79th_2]],
	[[5, CT.MIN79th]],
	
	[[5, CT.SUS_11_6_2]],
	[[-7, CT.SUS_11_5], [-7, CT.MAJ79th_2]],
	[[-7, CT.MAJ79th_2]],
	[[5, CT.SUS_11_5_2], [-1, CT.HENDRIX], [5, CT.SUS_11_6]],
	
	[[-5, CT.MAJ79th_2], [-1, CT.MIN79th]]
]
