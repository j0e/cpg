# Chord Progression Generator

Very simple chord progression generator written in python.\
Reads in chord progressions, builds up a table of rules and generates a chord progression based on that table of rules.\
(see input examples for more info)

Uses midiparse and midiutil -- see midiutil folder

```
usage: cpg.py [-b <numberofbars>] [-i <inputfoldername>] [-o <outputfile>] [-v]
```